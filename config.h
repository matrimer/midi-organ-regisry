// config.h for simple configuration

// This sets the midi channel, MIDI_CHANNEL_OMNI listens to all midi channels.
#define midi_channel 9


// The amount of rows and collumns in your LED matrix
#define ledrows 8 //  Pins C7 to C0 (Yes, backwards). Max 8
#define ledcols 8 // Pins A0 to A15. Max 16
#define ledcount 64 // The number of LEDs you have, should be ledrows*ledcols-1 (remember it starts counting at 0.) Setting this too high shouldn't be a problem.

#define startpitch 0 // The midi pitch of your first LED

#define address 0x70 // i2c address of the LED driver board
#define baudrate 9600


// TESTING/DEBUGGING:
//
// Uncomment the line underneath to check if LEDs are working
//#define checkLED 1
// The line underneath turns on light n-1. (You have to change n to the number off your LED)
//#define LEDon n
