// These are for communication with the adafruit HT16K33:
#include <Wire.h>
#include <Adafruit_GFX.h>
#include "Adafruit_LEDBackpack.h"
// These are for MIDI
#include <MIDI.h>
#include <USB-MIDI.h>
// Add the config file
#include "config.h"

int note[ledcount]; // Which light is on and which is off. 0=off, 1=on.

Adafruit_8x8matrix matrix = Adafruit_8x8matrix(); // initialize the LED matrix on the HT16K33
USBMIDI_CREATE_DEFAULT_INSTANCE(); // create a default midi instance

int ledwrite() // this function converts note[] to led[][] and writes led[][] to the LEDs.
{
  int i, j, k = 0;
  int led[ledcols][ledrows];
  matrix.clear();
  for ( i = 0; i < ledcols; i++)
  {
    for (j = 0; j < ledrows; j++)
    {
      led[i][j] = note[k];
      k++;
      matrix.drawPixel(j, i, led[i][j]); // Draw the contents of led[i][j] to the matrix
    }
  }
  matrix.writeDisplay(); // Write the matrix to the LEDs
  return k;
}

// This is run when a MIDI note on signal is recieved:
void noteon(byte channel, byte pitch, byte velocity)
{
//  note[pitch+startpitch] = 1; // 1 sets LED on
  note[pitch] = 1; // 1 sets LED on
  ledwrite();
}

// This is run when a MIDI note off signal is recieved:
void noteoff(byte channel, byte pitch, byte velocity)
{
//  note[pitch+startpitch] = 0; // 0 sets LED off
  note[pitch] = 0; // 0 sets LED off
  ledwrite();
}

void setup() // This is run once on every boot
{
  // Begin I2C communication with HT16K33:
  Serial.begin(baudrate);
  matrix.begin(address);
  #ifdef checkLED
    int i;
    for (i=0; i < ledcount; ++i)
    note[i] = checkLED;
  #endif
  #ifdef LEDon
    note[LEDon] = 1;
  #endif
  ledwrite(); // Turn all the LEDs off
  // Set up MIDI:
  MIDI.begin(midi_channel); // The argument here sets the MIDI channel to 9.
  // MIDI_CHANNEL_OMNI listens to all channels
  MIDI.turnThruOff();
  MIDI.setHandleNoteOn(noteon); // Run noteon when a MIDI note on signal is recieved
  MIDI.setHandleNoteOff(noteoff); // Run noteoff when a MIDI note off signal is recieved
}

void loop()
{
  MIDI.read();
}
